import os

from beaker import cache
from beaker.util import parse_cache_config_options

TMPDIR = os.getenv("AUTOPKGTEST_TMP")

options = {
    "cache.type": "file",
    "cache.data_dir": TMPDIR,
    "cache.lock_dir": TMPDIR,
}

cache_manager = cache.CacheManager(**parse_cache_config_options(options))

test_cache = cache_manager.get_cache("test", type="file", expire=5)

test_cache.set_value(key="there", value=1)

assert test_cache.get(key="there") == 1

try:
    test_cache.get(key="not_there")
except KeyError:
    print("OK!")
